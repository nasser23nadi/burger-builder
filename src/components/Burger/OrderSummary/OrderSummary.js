import React, {Component} from "react";
import PropTypes from "prop-types"

import Aux from "../../../hoc/Auxiliary/Auxiliary"
import Button from "../../UI/Button/Button";

class OrderSummary extends Component {

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("[Order Summary] updated")
    }

    render() {
        const ingredientSummary = Object.keys(this.props.ingredients)
            .map((igKey) => {
                return <li key={igKey}>
                    <span style={{textTransform: "capitalize"}}>{igKey}</span>: {this.props.ingredients[igKey]}</li>
            })

        return (
            <Aux>
                <h3>Your Order</h3>
                <p>A delicious burger with the following ingredients: </p>
                <ul>
                    {ingredientSummary}
                </ul>
                <p><strong>Total Price: {this.props.price.toFixed(2)}$</strong></p>
                <p>Continue to Checkout?</p>
                <Button btnType={"Danger"} clicked={this.props.cancelOrderSummary}>CANCEL</Button>
                <Button btnType={"Success"} clicked={this.props.continueOrderSummary}>CONTINUE</Button>
            </Aux>

        )
    }
}


OrderSummary.propTypes = {
    ingredients: PropTypes.object,
    price: PropTypes.number,
    cancelOrderSummary: PropTypes.func,
    continueOrderSummary: PropTypes.func
}

export default OrderSummary

