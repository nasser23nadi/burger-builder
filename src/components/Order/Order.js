import React from 'react';

import classes from "./Order.css"

const Order = (props) => {

    const ingredients = []
    for (let ingredientsName in props.ingredients) {
        ingredients.push({name: ingredientsName, amount: props.ingredients[ingredientsName]})
        // [{name:bacon,amount:2},{name:meat,amount:1},....]
    }

    const ingredientsOutput = ingredients.map((ing) => {
        return <span
            style={{
                textTransform: "capitalize",
                display: "inline-block",
                border: "2px solid #ccc",
                margin: "0 8px",
                padding: "5px"
            }}
            key={ing.name}>{ing.name} ({ing.amount}) </span>
    })

    return (
        <div className={classes.Order}>
            <p>Order ID : {props.orderId}</p>
            <p>Ingredients: {ingredientsOutput} </p>
            <p>Price:<strong> USD {props.price.toFixed(2)}</strong></p>
        </div>
    );
};

export default Order;