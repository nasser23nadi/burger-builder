import React, {Component} from "react";

import classes from "./Modal.css"
import Aux from "../../../hoc/Auxiliary/Auxiliary";
import Backdrop from "../Backdrop/Backdrop";

class modal extends Component {

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextProps.show !== this.props.show || nextProps.children!==this.props.children ){
            return true
        }
        return false
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("[Modal.js] updated")
    }

    render() {

        return (
            <Aux>
                <Backdrop show={this.props.show} cancel={this.props.cancelBackdrop}/>
                <div className={classes.Modal}
                     style={{
                         transform: this.props.show ? "translateY(0)" : "translateY(-100vh)",
                         opacity: this.props.show ? "1" : "0"
                     }}>
                    {this.props.children}
                </div>
            </Aux>
        )
    }
}

export default modal
