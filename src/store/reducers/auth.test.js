import reducer from "./auth";
import * as actionTypes from "../actions/actionTypes"
import {AUTH_SUCCESS} from "../actions/actionTypes";

describe("auth reducer", () => {
    it("should return initial state", () => {
        expect(reducer(undefined, {})).toEqual({
            token: null,
            userId: null,
            error: null,
            loading: false,
            authRedirectPath: "/"
        })
    })

    it("should store the token upon login", () => {
        expect(reducer({
            token: null,
            userId: null,
            error: null,
            loading: false,
            authRedirectPath: "/"
        }, {
            type:actionTypes.AUTH_SUCCESS,
            idToken: "some-token",
            userId: "user id"
        })).toEqual({
            token: "some-token",
            userId: "user id",
            error: null,
            loading: false,
            authRedirectPath: "/"
        })
    })

    it("should start loading",()=>{
        expect(reducer({
            token: null,
            userId: null,
            error: null,
            loading: false,
            authRedirectPath:"/"
        },{
            type:actionTypes.AUTH_START,
        })).toEqual({
            token: null,
            userId: null,
            error: null,
            loading: true,
            authRedirectPath:"/"
        })
    })
})
